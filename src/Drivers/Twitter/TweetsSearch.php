<?php

namespace MdelaRiva\ApiModel\Drivers\Twitter;

use MdelaRiva\ApiModel\Drivers\Basic;

class TweetsSearch extends Basic
{
    /**
     * Search operators
     * https://developer.twitter.com/en/docs/tweets/rules-and-filtering/overview/operators-by-product
     *
     * @var array
     */
    protected $operators = [
        'query', // custom
        'bio',
        'bio_location',
        'bio_name',
        'bounding_box',
        'contains',
        'followers_count',
        'friends_count',
        'from',
        'has:geo',
        'has:hashtags',
        'has:images',
        'has:lang',
        'has:links',
        'has:media',
        'has:mentions',
        'has:profile_geo',
        'has:symbols',
        'has:videos',
        'in_reply_to_status_id',
        'is:quote',
        'is:reply',
        'is:retweet',
        'is:verified',
        'keyword',
        'lang',
        'listed_count',
        'place_country',
        'place',
        'point_radius',
        'profile_bounding_box',
        'profile_country',
        'profile_locality',
        'profile_point_radius',
        'profile_region',
        'profile_subregion',
        'retweets_of_status_id',
        'retweets_of',
        'sample',
        'source',
        'statuses_count',
        'to',
        'url',
        'url_contains',
        'url_description',
        'url_title',
    ];

    /**
     * Unit of time for which count endpoint will be provided
     *
     * @var array
     */
    protected $buckets = [
        'day',
        'hour',
        'minute',
    ];

    /**
     * Returns the list of operators
     *
     * @return array
     */
    public function getWhereOperators()
    {
        return $this->operators;
    }

    /**
     * {@inheritdoc}
     */
    public function whereIn( string $field, array $value )
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function where( string $field, $operator, $value = null )
    {
        if( $value )
        {
            if( !is_string($value) )
            {
                throw new \Exception( 'Invalid value type' );
            }
        }
        elseif( !is_string($operator) )
        {
            throw new \Exception( 'Invalid value type' );
        }
        return parent::where( $field, $operator, $value );
    }

    /**
     * {@inheritdoc}
     */
    protected function buildWhere()
    {
        $whereRaw = $this->where;
        $whereStr = '';
        $where = [];

        if( $whereRaw )
        {
            foreach( $whereRaw as $whereItem )
            {
                if( !in_array( $whereItem['field'], $this->operators ) )
                {
                    throw new \Exception( 'Invalid operator' );
                }

                if( $whereStr )
                {
                    $whereStr .= ' ';
                }
                if( $whereItem['field'] == 'query' )
                {
                    $whereStr .= $whereItem['value'];
                }
                else
                {
                    $whereStr .= $whereItem['field'] . ':' . $whereItem['value'];
                }
            }
            $where = [ 'query' => $whereStr];
        }
        return $where;
    }

    /**
     * {@inheritdoc}
     */
    public function limit( int $perPage )
    {
        return $this->maxResults( $perPage );
    }

    /**
     * {@inheritdoc}
     */
    public function orderBy( string $field, string $direction = 'asc' )
    {
        // disabled
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function groupBy( ...$groups )
    {
        foreach( $groups as $group )
        {
            $this->bucket( $group );
        }
        return $this;
    }

    /**
     * Sets the -bucket- parameter value
     *
     * @param  string  $group
     * @return $this
     */
    public function bucket( string $group )
    {
        if( !in_array( $group, $this->buckets ) )
        {
            throw new \Exception( 'Invalid bucket type' );
        }
        $this->addParameter( 'bucket', $group );
        return $this;
    }

    /**
     * Sets the -tag- parameter value
     *
     * @param  string  $tag
     * @return $this
     */
    public function tag( string $tag )
    {
        $this->setParameter( 'tag', $tag );
        return $this;
    }

    /**
     * Sets the -fromDate- parameter value
     *
     * @param  DateTime  $fromDate
     * @return $this
     */
    public function fromDate( \DateTime $fromDate )
    {
        $this->setParameter( 'fromDate', $fromDate->format( 'YmdHi' ) );
        return $this;
    }

    /**
     * Sets the -toDate- parameter value
     *
     * @param  DateTime  $toDate
     * @return $this
     */
    public function toDate( \DateTime $toDate )
    {
        $this->setParameter( 'toDate', $toDate->format( 'YmdHi' ) );
        return $this;
    }

    /**
     * Sets the -maxResults- parameter value
     *
     * @param  string  $maxResults
     * @return $this
     */
    public function maxResults( int $maxResults )
    {
        $this->setParameter( 'maxResults', $maxResults );
        return $this;
    }

    /**
     * Sets the -next- parameter value
     *
     * @param  string  $next
     * @return $this
     */
    public function next( string $next )
    {
        $this->setParameter( 'next', $next );
        return $this;
    }
}