<?php

namespace MdelaRiva\ApiModel\Drivers\Youtube;

use MdelaRiva\ApiModel\Drivers\Basic;

class Search extends Basic
{
    /**
     * Search parameters
     * https://developers.google.com/youtube/v3/docs/search/list#parameters
     *
     * @var array
     */
    protected $parameters = [
        'part',
        'forContentOwner',
        'forDeveloper',
        'forMine',
        'relatedToVideoId',
        'channelId',
        'channelType',
        'eventType',
        'location',
        'locationRadius',
        'maxResults',
        'onBehalfOfContentOwner',
        'order',
        'pageToken',
        'publishedAfter',
        'publishedBefore',
        'q',
        'regionCode',
        'relevanceLanguage',
        'safeSearch',
        'topicId',
        'type',
        'videoCaption',
        'videoCategoryId',
        'videoDefinition',
        'videoDimension',
        'videoDuration',
        'videoEmbeddable',
        'videoLicense',
        'videoSyndicated',
        'videoType',
    ];

    /**
     * Returns the list of parameters
     *
     * @return array
     */
    public function getWhereParameters()
    {
        return $this->parameters;
    }

    /**
     * {@inheritdoc}
     */
    public function limit( int $perPage )
    {
        $this->setParameter( 'maxResults', $perPage );
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function orderBy( string $field, string $direction = 'asc' )
    {
        $allowed = [
            'date',
            'rating',
            'relevance',
            'title',
            'videoCount',
            'viewCount',
        ];
        if( !in_array( $field, $allowed ) )
        {
            throw new \Exception( 'invalid order parameter: ' . $field );
        }
        $this->addParameter( 'order', $field );

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function where( string $field, $operator, $value = null )
    {
        if( $value === null )
        {
            $value = $operator;
            $operator = null;
        }
        if( $value && !is_string($value) )
        {
            throw new \Exception( 'invalid value, not a string' );
        }
        if( !in_array( $field, $this->parameters ) )
        {
            throw new \Exception( 'Invalid parameter' );
        }
        return parent::where( $field, $operator, $value );
    }

    /**
     * {@inheritdoc}
     */
    public function groupBy( ...$groups )
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function select( $columns )
    {
        if( is_array($columns) )
        {
            $columns = implode( ',', $columns );
        }
        $this->addParameter( 'part', $columns );
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addSelect( $column )
    {
        if( is_array($column) )
        {
            $column = implode( ',', $column );
        }
        $whereKey = null;
        foreach( $this->where as $index => $whereItem )
        {
            if( $whereItem['field'] == 'part' )
            {
                $whereKey = $index;
                break;
            }
        }
        if( $whereKey )
        {
            $this->where[$whereKey]['value'] .= ',' . $column;
        }
        else
        {
            $this->select( $column );
        }
        return $this;
    }
}