<?php

namespace MdelaRiva\ApiModel\Drivers;

use MdelaRiva\ApiModel\Drivers\QueryInterface;

class Basic implements QueryInterface
{
    /**
     * Query parameters (except "where" parameter)
     *
     * @var array
     */
    private $parameters = [];

    /**
     * Where parameter
     *
     * @var array
     */
    protected $where = [];

    /**
     * {@inheritdoc}
     */
    public function toArray()
    {
        return $this->getParameters();
    }

    /**
     * {@inheritdoc}
     */
    public function whereIn( string $field, array $value )
    {
        return $this->where( $field, '=', $value );
    }

    /**
     * {@inheritdoc}
     */
    public function where( string $field, $operator, $value = null )
    {
        if( $value === null )
        {
            $value = $operator;
            $operator = null;
        }
        $this->where[] = [
            'field' => $field,
            'operator' => $operator,
            'value' => $value,
        ];

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function limit( int $perPage )
    {
        $this->setParameter( 'limit', $perPage );
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function orderBy( string $field, string $direction = '' )
    {
        $this->addParameter( 'orderBy', $field );
        if( $direction )
        {
            $this->addParameter( 'orderByDir', $direction );
        }

        return $this;
    }

    /**
     * Adds parameter to query
     *
     * @param  string  $parameter
     * @param  mixed  $value
     * @return MdelaRiva\ApiModel\Drivers\QueryInterface
     */
    protected function addParameter( string $parameter, $value )
    {
        if( isset($this->parameters[$parameter]) )
        {
            if( !is_array($this->parameters[$parameter]) )
            {
                $this->parameters[$parameter] = [$this->parameters[$parameter]];
            }
            $this->parameters[$parameter][] = $value;
        }
        else
        {
            $this->parameters[$parameter] = $value;
        }
    }

    /**
     * Sets parameter to query
     *
     * @param  string  $parameter
     * @param  mixed  $value
     * @return void
     */
    public function setParameter( string $parameter, $value )
    {
        $this->parameters[$parameter] = $value;
    }

    /**
     * Return all the parameters and values in query
     *
     * @return array
     */
    private function getParameters()
    {
        $parameters = array_merge( $this->parameters, $this->buildWhere() );
        return $parameters;
    }

    /**
     * Builds the where parameter for API
     *
     * @return array
     */
    protected function buildWhere()
    {
        $whereRaw = $this->where;
        $where = [];

        if( $whereRaw )
        {
            foreach( $whereRaw as $whereItem )
            {
                $fieldKey = $whereItem['field'];
                switch( $whereItem['operator'] )
                {
                    case '>':
                        $fieldKey .= '_gt';
                        break;
                    case '>=':
                        $fieldKey .= '_ge';
                        break;
                    case '<':
                        $fieldKey .= '_lt';
                        break;
                    case '<=':
                        $fieldKey .= '_le';
                        break;
                    case 'like':
                        $matches = [];
                        if( preg_match( '/^%(.*)%$/i', $whereItem['value'], $matches ) )
                        {
                            $fieldKey .= '_ct';
                            $whereItem['value'] = $matches[1];
                        }
                        elseif( preg_match( '/^(.*)%$/i', $whereItem['value'], $matches ) )
                        {
                            $fieldKey .= '_pr';
                            $whereItem['value'] = $matches[1];
                        }
                        elseif( preg_match( '/^%(.*)$/i', $whereItem['value'], $matches ) )
                        {
                            $fieldKey .= '_ap';
                            $whereItem['value'] = $matches[1];
                        }
                        break;
                }
                $where[ $fieldKey ] = $whereItem['value'];
            }
        }
        return $where;
    }

    /**
     * {@inheritdoc}
     */
    public function groupBy( ...$groups )
    {
        $this->addParameter( 'groupBy', $groups );
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function select( $columns )
    {
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addSelect( $column )
    {
        return $this;
    }
}