<?php

namespace MdelaRiva\ApiModel\Drivers;

interface QueryInterface
{
    /**
     * Converts the query to an array.
     *
     * @return array
     */
    public function toArray();

    /**
     * Query clause: whereIn
     *
     * @param  string  $field
     * @param  array  $value
     * @return $this
     */
    public function whereIn( string $field, array $value );

    /**
     * Query clause: where
     *
     * @param  string  $field
     * @param  array|string  $operator
     * @param  mixed  $value
     * @return $this
     */
    public function where( string $field, $operator, $value = null );

    /**
     * Query clause: limit
     *
     * @param  int  $perPage
     * @return $this
     */
    public function limit( int $perPage );

    /**
     * Query clause: orderBy
     *
     * @param  string  $field
     * @param  string  $direction
     * @return $this
     */
    public function orderBy( string $field, string $direction = 'asc' );

    /**
     * Query clause: groupBy
     *
     * @param  array|string  ...$groups
     * @return $this
     */
    public function groupBy(...$groups);

    /**
     * Query clause: select
     *
     * @param  array|mixed  $columns
     * @return $this
     */
    public function select( $columns );

    /**
     * Query clause: addSelect
     *
     * @param  array|mixed  $column
     * @return $this
     */
    public function addSelect( $column );

}