<?php

return [

    'default' => env('APIMODEL_CONNECTION', 'default'),

    'connections' => [

        'default' => [
            'driver' => null,
            'connector' => null,
            'persist_type' => 'cache',
            'url' => env( 'APIMODEL_HOST' ),
            'version' => env( 'APIMODEL_VERSION' ),
            'authorization' => [
                'type' => 'oauth_2',
                'credentials' => [
                    'client_id' => env( 'APIMODEL_CLIENT_ID' ),
                    'client_secret' => env( 'APIMODEL_CLIENT_SECRET' ),
                    'grant_type' => env( 'APIMODEL_CLIENT_GRANT_TYPE' ),
                    'scopes' => env( 'APIMODEL_SCOPES' ),
                ],
                'oauth_endpoint' => null,
                'oauth_base64header' => false,
                '401_refresh_token' => false,
            ],
            'wrappers' => [
                'collection' => [
                    'root' => 'data',
                    'pagination' => [
                        'total' => 'meta.total',
                    ]
                ],
            ],
            'verify' => env( 'APIMODEL_VERIFY' ),
            'timeout' => null,
            'allow_redirects' => FALSE,
            'debug' => FALSE,
            'cache' => [
                'enabled' => null,
                'path' => storage_path( 'framework/cache/data/apimodel/' ),
            ],
            'throttler' => [
                'throw_exception' => FALSE,
                'limits' => [],
            ],
        ],

    ],

];