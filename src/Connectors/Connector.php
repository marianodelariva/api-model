<?php

namespace MdelaRiva\ApiModel\Connectors;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Cache\FileStore;
use Illuminate\Cache\Repository;
use Illuminate\Support\Facades\Log;
use Monolog\Handler\NullHandler;
use MdelaRiva\ApiModel\Traits\ConfigurationTrait;
use MdelaRiva\ApiModel\Libraries\Throttler\Throttler;
use MdelaRiva\ApiModel\Exceptions\AuthenticationException;
use MdelaRiva\ApiModel\Exceptions\MissingConfigurationException;

class Connector
{
    use ConfigurationTrait{ getConfiguration as private; }

    /**
     * Default connection
     *
     * @var string
     */
    private $connection;

    /**
     * Logger
     *
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    /**
     * Auth type : No auth
     *
     * @var string
     */
    const AUTH_NOAUTH = 'no_auth';

    /**
     * Auth type : Api Key via Query param
     *
     * @var string
     */
    const AUTH_APIKEYQUERY = 'api_key_query';

    /**
     * Auth type : Api Key via Header param
     *
     * @var string
     */
    const AUTH_APIKEYHEADER = 'api_key_header';

    /**
     * Auth type : Bearer token
     *
     * @var string
     */
    const AUTH_BEARERTOKEN = 'bearer_token';

    /**
     * Auth type : Basic Auth
     *
     * @var string
     */
    const AUTH_BASICAUTH = 'basic_auth';

    /**
     * Auth type : Oauth 2
     *
     * @var string
     */
    const AUTH_OAUTH2 = 'oauth_2';

    /**
     * Oauth token request default endpoint
     *
     * @var string
     */
    const OAUTH_ENDPOINT = 'oauth/token';

    /**
     * Request endpoint
     *
     * @var string
     */
    private $endpoint;

    /**
     * HTTP client
     *
     * @var \Illuminate\Http\Client\PendingRequest
     */
    private $_client;

    /**
     * Cache client
     *
     * @var \Illuminate\Cache\Repository
     */
    private $_cache;

    /**
     * Cache enabled
     *
     * @var bool
     */
    public $cache = FALSE;

    /**
     * Cache refresh
     *
     * @var bool
     */
    public $cacheRefresh = FALSE;

    /**
     * Cache configuration
     *
     * @var array
     */
    private $cacheConfiguration = [];

    /**
     * Request parameters
     *
     * @var array
     */
    protected $parameters = [];

    /**
     * Initialize class
     *
     * @param  string|null  $endpoint
     * @param  string|null  $connection
     * @return void
     */
    public function __construct( $endpoint = null, $connection = null )
    {
        $this->endpoint = $endpoint;

        $loggerContext = [];
        // define connection
        $this->connection = $connection;
        // grouped connection
        if( $connections = $this->getConfiguration('connections') ) {
            // add grouped connection info in logger
            $loggerContext = [ 'network'=> $this->connection ];

            sort($connections);
            $groupedConnectionCache = $this->cache();
            $lastConnectionCacheKey = 'last_connection';
            if( $lastConnection = $groupedConnectionCache->get($lastConnectionCacheKey) ) {
                // find next connection
                $connectionIndex = array_search($lastConnection, $connections);
                if( $connectionIndex+1 === count($connections) ) {
                    // last element in the list, start over
                    $this->connection = $connections[0];
                } else {
                    // in the middle of the list
                    $this->connection = $connections[$connectionIndex+1];
                }
            } else {
                // no last connection, get the first connection in the list
                $this->connection = $connections[0];

            }
            // save last connection for next instance
            $groupedConnectionCache->forever( $lastConnectionCacheKey, $this->connection );
            // reset cache for connection
            $this->_cache = null;
        }

        // init logger
        $this->initLogger($loggerContext);

        // cache enabled flag
        $cacheEnabled = $this->getCacheConfiguration( 'enabled' );
        if( is_bool( $cacheEnabled ) )
        {
            $this->cache = $cacheEnabled;
        }

        $this->setClient();
    }

    /**
     * Set a new HTTP client instance
     *
     * @return void
     */
    private function setClient()
    {
        $configuration = [
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ];
        $this->_client = Http::withHeaders( $configuration );
        if( $this->getConfiguration( 'timeout' ) )
        {
            $this->_client->timeout( $this->getConfiguration( 'timeout' ) );
        }
    }

    /**
     * Returns endpoint url
     *
     * @return string
     */
    public function getEndpoint()
    {
        return $this->endpoint;
    }

    /**
     * Sets endpoint url
     *
     * @param  string  $endpoint
     * @return void
     */
    public function setEndpoint( $endpoint )
    {
        $this->endpoint = $endpoint;
    }

    /**
     * Returns base url
     *
     * @return string
     */
    private function getUrl()
    {
        return $this->getConfiguration( 'url' );
    }

    /**
     * Adds new parameter in request
     *
     * @param  string  $parameter
     * @param  mixed  $value
     * @return void
     */
    public function addParameter( string $parameter, $value )
    {
        if( isset($this->parameters[$parameter]) )
        {
            $this->parameters[$parameter][] = $value;
        }
        else
        {
            $this->parameters[$parameter] = $value;
        }
    }

    /**
     * Merge new parameters with existing parameters
     *
     * @param  array  $value
     * @return void
     */
    public function mergeParameters( array $value )
    {
        if( !is_array($this->parameters) )
        {
            if( $this->parameters )
            {
                $this->parameters = [$this->parameters];
            }
            else
            {
                $this->parameters = [];
            }
        }
        $this->parameters = array_merge_recursive( $this->parameters, $value );
    }

    /**
     * Replace new parameters with existing parameters
     *
     * @param  array  $parameters
     * @return void
     */
    public function replaceParameters( array $parameters )
    {
        foreach( $parameters as $valueName => $value )
        {
            $this->parameters[$valueName] = $value;
        }
    }

    /**
     * Sets an specific parameter
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function setParameter( string $key, $value )
    {
        $this->parameters[$key] = $value;
    }

    /**
     * Set all the parameters at once
     *
     * @param  array  $value
     * @return void
     */
    protected function setParameters( array $value )
    {
        $this->parameters = $value;
    }

    /**
     * Returns a specific parameter
     *
     * @param  string  $key
     * @return mixed|null
     */
    protected function getParameter( string $key )
    {
        return $this->parameters[$key] ?? null;
    }

    /**
     * Oauth: Refresh API Access Token
     *
     * @return stdClass|null
     *
     * @throws Exception
     */
    private function oauthRefreshToken()
    {
        $authentication = $this->getOauthAuthentication();
        if( empty($authentication->refresh_token) )
        {
            $authentication = $this->oauthAuthenticate();
            if( $authentication )
            {
                return $authentication;
            }
            throw new AuthenticationException();
        }
        $authorization = $this->getConfiguration( 'authorization' );
        $credentials = $authorization['credentials'];
        $self = clone $this;
        $authenticationBody = [
            'grant_type' => 'refresh_token',
            'client_id' => $credentials['client_id'],
            'client_secret' => $credentials['client_secret'],
            'refresh_token' => $authentication->refresh_token,
        ];
        $self->setParameters( $authenticationBody );
        $oauthUrl = $authorization['oauth_endpoint'] ?? self::OAUTH_ENDPOINT;
        $authenticationResponse = $self->sendRequest( $oauthUrl, 'POST', FALSE );
        if( !$authenticationResponse )
        {
            throw new AuthenticationException();
        }
        $this->setOauthAuthentication( $authenticationResponse );

        return $authenticationResponse;
    }

    /**
     * Oauth: API User authentication
     *
     * @return stdClass|null
     *
     * @throws Exception
     */
    private function oauthAuthenticate()
    {
        $authorization = $this->getConfiguration( 'authorization' );
        $credentials = $authorization['credentials'];
        if( empty($credentials['client_id']) || empty($credentials['client_secret']) )
        {
            throw new MissingConfigurationException();
        }

        $self = clone $this;
        $authenticationHeader = [];
        $authenticationBody = [];
        if( isset($authorization['oauth_base64header']) && $authorization['oauth_base64header'] === TRUE )
        {
            $authenticationHeader['Authorization'] = 'Basic ' . base64_encode( $credentials['client_id'] . ':' . $credentials['client_secret'] );
        }
        else
        {
            $authenticationBody['client_id'] = $credentials['client_id'];
            $authenticationBody['client_secret'] = $credentials['client_secret'];
        }
        unset($credentials['client_id']);
        unset($credentials['client_secret']);
        if( $credentials )
        {
            $authenticationBody = array_merge( $authenticationBody, $credentials );
        }
        $self->setParameters( $authenticationBody );
        $oauthUrl = $authorization['oauth_endpoint'] ?? self::OAUTH_ENDPOINT;

        $authenticationResponse = $self->sendRequest( $oauthUrl, 'POST', FALSE, $authenticationHeader );
        if( !$authenticationResponse )
        {
            throw new AuthenticationException();
        }
        $this->setOauthAuthentication( $authenticationResponse );

        return $authenticationResponse;
    }

    /**
     * Oauth: Sets authentication information
     *
     * @param  mixed  $authentication
     * @return void
     */
    public function setOauthAuthentication( $authentication )
    {
        switch( $this->getConfiguration( 'persist_type' ) )
        {
            case 'cache':
                Cache::put( $this->generateAuthenticationKey(), $authentication );
                return;

            case 'session':
                session( [ $this->generateAuthenticationKey() => $authentication ] );
                return;

            default:
                throw new MissingConfigurationException( 'Invalid persist type' );
        }
    }


    /**
     * Generate a unique key for connection authentication cache saving
     *
     * @return string
     */
    private function generateAuthenticationKey()
    {
        $connectionKey = 'connection_' . md5(
            $this->connection . '_' .
            $this->getUrl()
        );
        return $connectionKey;
    }

    /**
     * Oauth: Returns authentication information
     *
     * @return stdClass
     *
     * @throws Exception
     */
    private function getOauthAuthentication()
    {
        switch( $this->getConfiguration( 'persist_type' ) )
        {
            case 'cache':
                return Cache::get( $this->generateAuthenticationKey() );

            case 'session':
                return session( $this->generateAuthenticationKey() );

            default:
                throw new MissingConfigurationException( 'Invalid persist type' );
        }
    }

    /**
     * Oauth: Returns Access Token
     *
     * @return string|null
     */
    private function getOauthAccessToken()
    {
        $authentication = $this->getOauthAuthentication();
        $accessToken = $authentication->access_token ?? NULL ;
        if( !$accessToken )
        {
            $authentication = $this->oauthAuthenticate();
            if( empty($authentication->access_token) )
            {
                throw new AuthenticationException();
            }
            $accessToken = $authentication->access_token;
        }
        return $accessToken;
    }

    /**
     * Send request to remote
     *
     * @param  string $endpoint
     * @param  string $method
     * @param  bool $includeAuthorization
     * @param  array $headers
     * @return mixed
     *
     * @throws Exception
     */
    public function sendRequest( string $endpoint, string $method = 'GET', bool $includeAuthorization = TRUE, array $headers = [] )
    {
        // cache - is enabled?
        $cache = $this->cache;
        if( $method != 'GET' ||
            $includeAuthorization === FALSE ||
            $this->getCacheConfiguration( 'enabled' ) === FALSE )
        {
            $cache = FALSE;
        }
        // base request parameters
        $requestParams = [
            'headers' => [],
            'query' => [],
            'form_params' => [],
        ];
        // set authorization
        if( $includeAuthorization === TRUE )
        {
            switch( $this->getConfiguration( 'authorization.type' ) )
            {
                case '':
                    throw new MissingConfigurationException( 'Empty authorization type' );

                case self::AUTH_NOAUTH:
                    break;

                case self::AUTH_APIKEYQUERY:
                    $credentialsQuery = $this->getConfiguration( 'authorization.credentials.query' );
                    if( !$credentialsQuery || !is_array($credentialsQuery) )
                    {
                        throw new MissingConfigurationException();
                    }
                    $requestParams['query'] = array_merge( $requestParams['query'], $credentialsQuery );
                    break;

                case self::AUTH_APIKEYHEADER:
                    $credentialsHeaders = $this->getConfiguration( 'authorization.credentials.headers' );
                    if( !$credentialsHeaders || !is_array($credentialsHeaders) )
                    {
                        throw new MissingConfigurationException();
                    }
                    $requestParams['headers'] = array_merge( $requestParams['headers'], $credentialsHeaders );
                    break;

                case self::AUTH_BEARERTOKEN:
                    $authToken = $this->getConfiguration( 'authorization.credentials.token' );
                    if( !$authToken )
                    {
                        throw new MissingConfigurationException();
                    }
                    $requestParams['headers']['Authorization'] = 'Bearer ' . $authToken;
                    break;

                case self::AUTH_BASICAUTH:
                    $authUsername = $this->getConfiguration( 'authorization.credentials.username' );
                    $authPassword = $this->getConfiguration( 'authorization.credentials.password' );
                    if( !$authUsername | !$authPassword )
                    {
                        throw new MissingConfigurationException();
                    }
                    $requestParams['headers']['Authorization'] = 'Basic ' . base64_encode( $authUsername . ':' . $authPassword );
                    break;

                case self::AUTH_OAUTH2:
                    $accessToken = $this->getOauthAccessToken();
                    if( !$accessToken )
                    {
                        return FALSE;
                    }
                    $requestParams['headers']['Authorization'] = 'Bearer ' . $accessToken;
                    break;

                default:
                    throw new MissingConfigurationException( 'Invalid authorization type.' );
            }
        }
        // headers
        if( $headers )
        {
            $requestParams['headers'] = array_merge( $requestParams['headers'], $headers );
        }
        // url full build
        $url = $this->getUrl();
        if( substr( $url, -1, 1 ) !== '/' )
        {
            $url .= '/';
        }
        if( $includeAuthorization === TRUE )
        {
            $url .= $this->getConfiguration( 'version' );
        }
        if( substr( $url, -1, 1 ) !== '/' && substr( $endpoint, 0, 1 ) !== '/' )
        {
            $url .= '/';
        }
        $url .= $endpoint;

        // set parameters in request configuration
        $paramFieldType = ( $method == 'POST' ? 'form_params' : 'query' );
        foreach( $this->parameters as $parameterKey => $parameterValue )
        {
            $requestParams[$paramFieldType][$parameterKey] = $parameterValue;
        }

        // fix: if null value, change it to an empty string
        array_walk_recursive(
            $requestParams['query'],
            function ( &$item,$key )
            {
                $item = ( is_null($item) ? '' : $item );
            }
        );
        array_walk_recursive(
            $requestParams['form_params'],
            function ( &$item,$key )
            {
                $item = ( is_null($item) ? '' : $item );
            }
        );

        // cache
        $cacheKey = self::cacheKeyGenerator( $url, $requestParams['query'] );
        if( $cache === TRUE && $this->cacheRefresh === FALSE )
        {
            $cacheResponse = $this->cache()->get( $cacheKey );
            if( $cacheResponse !== null )
            {
                $this->log('cache',$method . ' | ' . $url . ' | ' . json_encode($requestParams['query']));
                return $cacheResponse;
            }
        }

        // throttler
        if( $includeAuthorization !== TRUE )
        {
            $throttleConfiguration = $this->getConfiguration( 'throttler' ) ?? [];
            $throttleKey = 'throttle_' . $this->generateAuthenticationKey();
            $throttler = new Throttler( $throttleKey, $throttleConfiguration );
            if( $throttler->check() === FALSE )
            {
                return FALSE;
            }
        }

        // debug mode
        $requestParams['debug'] = $this->getConfiguration( 'debug' );

        // do not allow redirects
        $requestParams['allow_redirects'] = $this->getConfiguration( 'allow_redirects' );

        // ssl verification
        if( $this->getConfiguration( 'verify' ) !== null )
        {
            $requestParams['verify'] = $this->getConfiguration( 'verify' );
        }
        // make request
        $this->log('requests',$method . ' | ' . $url . ' | ' . json_encode($requestParams['query']));
        $response = $this->_client->send( $method, $url, $requestParams );
        if( $response->failed() )
        {
            $responseStatusCode = $response->getStatusCode();
            // preparing response with the error
            if( request()->ajax() )
            {
                $response->header('Content-Type','application/json');
            }
            // are we on the begining of this journey? aka, "requesting access"
            if( $includeAuthorization === FALSE )
            {
                return FALSE;
            }
            switch( $responseStatusCode )
            {
                case 401:
                    // authorization error
                    if( $includeAuthorization )
                    {
                        if( $this->getConfiguration( 'authorization.type' ) === self::AUTH_OAUTH2
                        && $this->getConfiguration( 'authorization.401_refresh_token' ) === true )
                        {
                            // refresh token
                            $refreshedToken = $this->oauthRefreshToken();
                            if( $refreshedToken )
                            {
                                return $this->sendRequest( $endpoint, $method, $includeAuthorization );
                            }
                        }
                    }
                    break;
                    
                case 404:
                    return FALSE;
            }
            $response->throw();
        }
        // delete methods will return bool type
        if( $method == 'DELETE' )
        {
            $responseBody = TRUE;
        }
        else
        {
            $responseBody = json_decode( (string) $response->getBody() );
            // not a valid response format?
            if( !$responseBody )
            {
                $responseBody = $response->getBody()->__toString();
            }
            // empty ugly buggy response?
            if( $responseBody == '[]' )
            {
                $responseBody = [];
            }
        }
        // cache - store
        if( $cache === TRUE )
        {
            $this->cache()->put( $cacheKey, $responseBody, $this->getCacheConfiguration( 'ttl' ) );
        }

        return $responseBody;
    }

    /**
     * Generates a key for cache saving
     *
     * @param  string $url
     * @param  array $query
     * @return string
     */
    private static function cacheKeyGenerator( string $url, array $query = [] )
    {
        ksort($query);
        $prefix = 'am_';
        $queryString = str_replace( '"', '', json_encode( $query ) );
        $hashed = md5( $url . $queryString );
        $cacheKey = $prefix . $hashed;

        return $cacheKey;
    }

    /**
     * Cache instance
     *
     * @return Illuminate\Cache\Repository
     */
    private function cache()
    {
        if( !$this->_cache )
        {
            $path = $this->getCacheConfiguration( 'path' );
            if( !$path )
            {
                throw new MissingConfigurationException( 'Cache path not found in config' );
            }
            $cacheDirectory = $path . $this->connection . '/' . $this->getEndpoint();
            $fileSystem = new Filesystem();
            $fileStore = new FileStore( $fileSystem, $cacheDirectory );
            $this->_cache = new Repository( $fileStore );
        }

        return $this->_cache;
    }

    /**
     * Returns specific key or entire cache configuration
     *
     * @param  string|null  $key
     * @return mixed
     */
    private function getCacheConfiguration( string $key = null )
    {
        $config = array_replace( ( $this->getConfiguration( 'cache' ) ?: [] ), $this->cacheConfiguration );
        if( $key !== NULL )
        {
            $value = ( $config[$key] ?? null );
        }
        else
        {
            $value = $config;
        }

        return $value;
    }

    /**
     * Flushes cache for an specific endpoint
     *
     * @return void
     */
    public function cacheFlush()
    {
        $this->cache()->flush();
    }

    /**
     * Initialize logger
     *
     * @param array $context
     */
    private function initLogger( array $context = [] ) 
    {
        // logging disabled
        $config = $this->getConfiguration( 'logging' ) ?: [];
        if( !isset($config['enabled']) || $config['enabled'] !== true ) {
            $this->logger = Log::build([
                'driver' => 'monolog',
                'handler' => NullHandler::class,
            ]);
            return;
        }

        $channelLogConfig = [
            'driver' => 'daily',
            'days' => 7,
            'path' => storage_path('logs/' . strtolower('apimodel-' . $this->connection) . '.log'),
        ];

        $this->logger = Log::build($channelLogConfig)
            ->withContext($context);
    }

    /**
     * Log message
     *
     * @param string $type
     * @param string $message
     */
    private function log( string $type, string $message ) 
    {
        $config = $this->getConfiguration( 'logging' ) ?: [];
        // only log this type of logs
        if( !empty($config['type']) && $config['type'] !== $type ) {
            return;
        }

        $this->logger
            ->withContext([
                'type' => $type
            ])
            ->info( $message );
    }

}