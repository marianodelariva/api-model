<?php

namespace MdelaRiva\ApiModel\Traits;

use MdelaRiva\ApiModel\Exceptions\MissingConfigurationException;

trait ConfigurationTrait
{
    /**
     * Service configuration
     *
     * @var array
     */
    private $_configuration = [];

    /**
     * Instance default configuration
     *
     * @var array
     */
    private $_configurationDefaults = [
        'driver'            => \MdelaRiva\ApiModel\Drivers\Basic::class,
        'connector'         => \MdelaRiva\ApiModel\Connectors\Connector::class,
        'persist_type'      => 'cache',
        'debug'             => FALSE,
        'allow_redirects'   => FALSE,
        'verify'            => null,
        'cache'             => [
            'ttl' => 86400,
            'enabled' => null,
        ],
    ];

    /**
     * Initialize configuration
     *
     * @return void
     */
    private function discoverConfiguration()
    {
        $configuration = config( 'apimodel' );
        if( !$configuration )
        {
            throw new MissingConfigurationException( 'Config file not found' );
        }
        if( empty($configuration['default']) && !$this->connection )
        {
            throw new MissingConfigurationException( 'Missing connection' );
        }
        $this->_configuration = $configuration;
    }

    /**
     * Returns specific key or entire configuration
     *
     * @param  string|null  $key
     * @return array|null
     */
    private function getConfiguration( string $key = null, string $connection = null )
    {
        if( !$this->_configuration )
        {
            $this->discoverConfiguration();
        }
        $configurationFull = $this->_configuration;
        if( $connection )
        {
            $configurationFull['default'] = $connection;
        }
        elseif( !empty($this->connection) )
        {
            $configurationFull['default'] = $this->connection;
        }
        if( !isset($configurationFull['connections'][$configurationFull['default']]) )
        {
            throw new MissingConfigurationException( 'Selected connection not found in config' );
        }
        $configuration = $configurationFull['connections'][$configurationFull['default']];
        if( !empty($this->_configurationDefaults) )
        {
            // default
            $this->_configurationDefaults['cache']['path'] = storage_path( 'framework/cache/data/apimodel/' );
            //
            $configuration = array_replace_recursive(
                $this->_configurationDefaults,
                $configuration
            );
        }
        if( $key )
        {
            foreach( explode( '.', $key ) as $keyPart )
            {
                $configuration = ( $configuration[$keyPart] ?? NULL );
                if( $configuration === NULL )
                {
                    return NULL;
                }
            }
        }
        return $configuration;
    }

    /**
     * Merge new configuration with existing ones
     *
     * @param  array  $newConfiguration
     * @return void
     */
    public function mergeConfiguration( array $newConfiguration )
    {
        if( !$this->_configuration )
        {
            $this->discoverConfiguration();
        }
        $configurationFull = $this->_configuration;
        if( !empty($this->connection) )
        {
            $configurationFull['default'] = $this->connection;
        }
        if( !isset($configurationFull['connections'][$configurationFull['default']]) )
        {
            throw new MissingConfigurationException( 'Connection not found' );
        }
        $configuration = array_replace_recursive( $configurationFull['connections'][$configurationFull['default']], $newConfiguration );
        $this->_configuration['connections'][$configurationFull['default']] = $configuration;
    }
}