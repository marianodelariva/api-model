<?php

namespace MdelaRiva\ApiModel;

use ArrayAccess;
use JsonSerializable;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Pagination\Paginator;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Contracts\Support\Jsonable;
use Illuminate\Support\Str;
use Illuminate\Support\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\MassAssignmentException;
use Illuminate\Database\Eloquent\JsonEncodingException;
use Illuminate\Database\Eloquent\Concerns\HasAttributes;
use Illuminate\Database\Eloquent\Concerns\HidesAttributes;
use Illuminate\Database\Eloquent\Concerns\GuardsAttributes;
use Illuminate\Database\Eloquent\Concerns\HasTimestamps;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;

use MdelaRiva\ApiModel\Drivers\QueryInterface;
use MdelaRiva\ApiModel\Connectors\Connector;
use MdelaRiva\ApiModel\Traits\ConfigurationTrait;

abstract class ApiModelAbstract implements Arrayable, ArrayAccess, Jsonable, JsonSerializable
{
    use HasAttributes,
        HidesAttributes,
        GuardsAttributes,
        HasTimestamps,
        HasRelationships,
        ConfigurationTrait{ getConfiguration as private; }

    /**
     * Default connection
     *
     * @var string
     */
    protected $connection;

    /**
     * API connection
     *
     * @var MdelaRiva\ApiModel\Connectors\Connector
     */
    private $_connection;

    /**
     * Cache enabled flag
     *
     * @var bool
     */
    public $cache;

    /**
     * Cache refresh query flag
     *
     * @var bool
     */
    public $cacheRefresh;

    /**
     * Cache TTL
     *
     * @var int
     */
    public $cacheTtl;

    /**
     * Default endpoint
     *
     * @var string
     */
    protected $endpoint;

    /**
     * Default driver
     *
     * @var string
     */
    protected $driver;

    /**
     * Query Builder
     *
     * @var MdelaRiva\ApiModel\Drivers\QueryInterface
     */
    private $query;

    /**
     * The name of the "created at" column.
     *
     * @var string
     */
    const CREATED_AT = 'created_at';

    /**
     * The name of the "updated at" column.
     *
     * @var string
     */
    const UPDATED_AT = 'updated_at';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The number of models to return for pagination.
     *
     * @var int
     */
    protected $perPage = 20;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * Create a new ApiModel model instance.
     *
     * @param  array  $attributes
     * @return void
     */
    public function __construct(array $attributes = [])
    {
        $this->initConnection();

        $this->initQuery();

        $this->discoverEndpoint();

        $this->dateFormat = 'Y-m-d H:i:s';

        $this->syncOriginal();

        $this->fill($attributes);
    }

    /**
     * Returns connection object
     *
     * @return MdelaRiva\ApiModel\Connectors\Connector|null
     */
    protected function getConnection()
    {
        if( $this->_connection )
        {
            if( is_bool( $this->cache ) )
            {
                $this->_connection->cache = $this->cache;
            }
            if( is_bool( $this->cacheRefresh ) )
            {
                $this->_connection->cacheRefresh = $this->cacheRefresh;
            }
            if( is_int( $this->cacheTtl ) )
            {
                $this->_connection->mergeConfiguration( [ 'cache' => [ 'ttl' => $this->cacheTtl ] ] );
            }
        }
        return $this->_connection;
    }

    /**
     * Sets connection instance
     *
     * @param  MdelaRiva\ApiModel\Connectors\Connector  $connection
     * @return void
     */
    protected function setConnection( Connector $connection )
    {
        $this->_connection = $connection;
    }

    /**
     * Initialize connection
     *
     * @return void
     */
    private function initConnection()
    {
        if( !$this->getConnection() )
        {
            // pasar a config
            $connector = $this->getConfiguration( 'connector', $this->connection );
            $this->setConnection( new $connector( null, $this->connection ) );
        }
    }

    /**
     * Initialize query builder
     *
     * @return void
     */
    private function initQuery()
    {
        if( $this->driver )
        {
            $driverClass = $this->driver;
        }
        else
        {
            $driverClass = $this->getConfiguration( 'driver' );
        }
        $driver = new $driverClass();

        $implements = class_implements( $driver );
        if( !isset($implements[QueryInterface::class]) )
        {
            throw new \Exception( 'Driver must be an interface of QueryInterface' );
        }

        $this->query = $driver;
    }

    /**
     * Returns query instance
     *
     * @return MdelaRiva\ApiModel\Drivers\QueryInterface
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * Get the table associated with the model.
     *
     * @return string
     */
    public function getTable()
    {
        return '';
    }

    /**
     * Define endpoint using model name
     *
     * @return void
     */
    private function discoverEndpoint()
    {
        if( $this->endpoint )
        {
            $className = $this->endpoint;
        }
        else
        {
            $className = Str::snake( Str::pluralStudly( class_basename( $this ) ) );
        }

        $this->setEndpoint( $className );
    }

    /**
     * Returns endpoint
     *
     * @return string
     */
    public function getEndpoint()
    {
        return $this->getConnection()->getEndpoint();
    }

    /**
     * Sets the endpoint
     *
     * @param  string  $endpoint
     * @return void
     */
    public function setEndpoint( string $endpoint )
    {
        $this->getConnection()->setEndpoint( $endpoint );
    }

    /**
     * Fill the model with an array of attributes.
     *
     * @param  array  $attributes
     * @return $this
     *
     * @throws \Illuminate\Database\Eloquent\MassAssignmentException
     */
    public function fill(array $attributes)
    {
        $totallyGuarded = $this->totallyGuarded();

        foreach ($this->fillableFromArray($attributes) as $key => $value) {
            $key = $this->removeTableFromKey($key);

            // The developers may choose to place some attributes in the "fillable" array
            // which means only those attributes may be set through mass assignment to
            // the model, and all others will just get ignored for security reasons.
            if ($this->isFillable($key)) {
                $this->setAttribute($key, $value);
            } elseif ($totallyGuarded) {
                throw new MassAssignmentException(sprintf(
                    'Add [%s] to fillable property to allow mass assignment on [%s].',
                    $key, get_class($this)
                ));
            }
        }

        return $this;
    }

    /**
     * Fill the model with an array of attributes. Force mass assignment.
     *
     * @param  array  $attributes
     * @return $this
     */
    public function forceFill(array $attributes)
    {
        return static::unguarded(function () use ($attributes) {
            return $this->fill($attributes);
        });
    }

    /**
     * Remove the table name from a given key.
     *
     * @param  string  $key
     * @return string
     */
    protected function removeTableFromKey($key)
    {
        return Str::contains($key, '.') ? last(explode('.', $key)) : $key;
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return $this->incrementing;
    }

    /**
     * Set whether IDs are incrementing.
     *
     * @param  bool  $value
     * @return $this
     */
    public function setIncrementing($value)
    {
        $this->incrementing = $value;

        return $this;
    }

    /**
     * Dynamically retrieve attributes on the model.
     *
     * @param  string  $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getAttribute($key);
    }

    /**
     * Dynamically set attributes on the model.
     *
     * @param  string  $key
     * @param  mixed  $value
     * @return void
     */
    public function __set($key, $value)
    {
        $this->setAttribute($key, $value);
    }

    /**
     * Convert the model instance to an array.
     *
     * @return array
     */
    public function toArray()
    {
        return array_merge($this->attributesToArray(), $this->relationsToArray());
    }

    /**
     * Convert the model instance to JSON.
     *
     * @param  int  $options
     * @return string
     *
     * @throws \Illuminate\Database\Eloquent\JsonEncodingException
     */
    public function toJson($options = 0)
    {
        $json = json_encode($this->jsonSerialize(), $options);

        if (JSON_ERROR_NONE !== json_last_error()) {
            throw JsonEncodingException::forModel($this, json_last_error_msg());
        }

        return $json;
    }

    /**
     * Convert the object into something JSON serializable.
     *
     * @return array
     */
    #[\ReturnTypeWillChange]
    public function jsonSerialize()
    {
        return $this->toArray();
    }

    /**
     * Determine if the given attribute exists.
     *
     * @param  mixed  $offset
     * @return bool
     */
    #[\ReturnTypeWillChange]
    public function offsetExists($offset)
    {
        return ! is_null($this->getAttribute($offset));
    }

    /**
     * Get the value for a given offset.
     *
     * @param  mixed  $offset
     * @return mixed
     */
    #[\ReturnTypeWillChange]
    public function offsetGet($offset)
    {
        return $this->getAttribute($offset);
    }

    /**
     * Set the value for a given offset.
     *
     * @param  mixed  $offset
     * @param  mixed  $value
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetSet($offset, $value)
    {
        $this->setAttribute($offset, $value);
    }

    /**
     * Unset the value for a given offset.
     *
     * @param  mixed  $offset
     * @return void
     */
    #[\ReturnTypeWillChange]
    public function offsetUnset($offset)
    {
        unset($this->attributes[$offset], $this->relations[$offset]);
    }

    /**
     * Get the primary key for the model.
     *
     * @return string
     */
    public function getKeyName()
    {
        return $this->primaryKey;
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string  $key
     * @return bool
     */
    public function __isset($key)
    {
        return $this->offsetExists($key);
    }

    /**
     * Unset an attribute on the model.
     *
     * @param  string  $key
     * @return void
     */
    public function __unset($key)
    {
        $this->offsetUnset($key);
    }

    /**
     * Handle dynamic static method calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return mixed
     */
    public static function __callStatic($method, $parameters)
    {
        return (new static)->$method(...$parameters);
    }

    /**
     * Convert the model to its string representation.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->toJson();
    }

    /**
     * Handle dynamic calls into the method.
     *
     * @param  string  $method
     * @param  array  $parameters
     * @return $this
     */
    public function __call( $method, $parameters )
    {
        $this->getQuery()->$method(...$parameters);
        return $this;
    }

    /**
     * Translate remote results into models
     *
     * @param  mixed  $response
     * @return mixed
     */
    protected function responseTranslator( $response )
    {
        switch( gettype($response) )
        {
            case 'array':
                $list = [];
                foreach( $response as $responseItem )
                {
                    $object = clone $this;
                    $object->forceFill( (array)$responseItem );
                    $list[] = $object;
                }
                return new Collection( $list );
            case 'object':
                if( get_class( $response ) != 'stdClass' )
                {
                    return $this;
                }
                // collection
                $collectionWrapper = $this->getConfiguration( 'wrappers.collection.root' );
                if( !empty($collectionWrapper) )
                {
                    $isCollection = TRUE;
                    $responseResults = $response;
                    foreach( explode( '.', $collectionWrapper ) as $collectionWrapperPart )
                    {
                        if( isset($responseResults->$collectionWrapperPart) )
                        {
                            $responseResults = $responseResults->$collectionWrapperPart;
                        }
                        else
                        {
                            $isCollection = FALSE;
                            break;
                        }
                    }
                    if( $isCollection )
                    {
                        $list = [];
                        if( $responseResults && is_array($responseResults) )
                        {
                            foreach( $responseResults as $responseItem )
                            {
                                $object = clone $this;
                                $object->forceFill( (array)$responseItem );
                                $list[] = $object;
                            }
                        }
                        return new Collection( $list );
                    }
                }
                // model object
                $object = clone $this;
                $object->forceFill( (array)$response );
                foreach( $object->toArray() as $attibute => $value )
                {
                    if( is_object($value) )
                    {
                        $modelName = '\App\Models\\' . Str::studly( Str::singular( $attibute ) );
                        if( class_exists($modelName) )
                        {
                            $attributeMolde = new $modelName();
                            $attributeMolde->forceFill( (array)$value );
                            $object->$attibute = $attributeMolde;
                        }
                    }
                }
                return $object;
        }
        return $response;
    }

    /**
     * Get the first record matching the attributes or instantiate it.
     *
     * @param  array  $where
     * @return object
     */
    public static function firstOrNew( array $where )
    {
        $query = self::query();
        foreach( $where as $whereField => $whereValue )
        {
            $query->where( $whereField, $whereValue );
        }
        $object = $query->first();
        if( !$object )
        {
            $object = new static;
            $object->forceFill( $where );
        }
        return $object;
    }

    /**
     * Send PUT/POST request with model information to API
     *
     * @return mixed
     */
    public function save()
    {
        $endpoint = $this->getEndpoint();
        $this->getConnection()->setParameter( 'data', $this->toArray() );
        if( isset($this->id) && $this->id )
        {
            $endpoint .= '/' . $this->id;
            $response = $this->getConnection()->sendRequest( $endpoint, 'PUT' );
        }
        else
        {
            $response = $this->getConnection()->sendRequest( $endpoint, 'POST' );
        }
        if( $response !== NULL )
        {
            $responseObj = $this->responseTranslator( $response );
            if( is_object($responseObj) )
            {
                $this->forceFill( $responseObj->toArray() );
                return $responseObj;
            }
            return $responseObj;
        }
        return null;
    }

    /**
     * Get the query grammar used by the connection.
     *
     * @return self
     */
    public function getQueryGrammar()
    {
        return self::newQuery();
    }

    /**
     * Get the format for dates.
     *
     * @return string
     */
    public function getDateFormat()
    {
        return $this->dateFormat;
    }
    /**
     * Begin querying the model.
     *
     * @return self
     */
    public static function query()
    {
        return self::newQuery();
    }

    /**
     * Get a new query builder
     *
     * @return self
     */
    public static function newQuery()
    {
        return new static;
    }

    /**
     * Get a new query builder, using cache
     *
     * @return self
     */
    public static function cacheQuery( bool $refresh = FALSE, int $ttl = null )
    {
        $cacheQuery = self::query();
        $cacheQuery->cache = TRUE;
        $cacheQuery->cacheRefresh = $refresh;
        if( $ttl )
        {
            $cacheQuery->cacheTtl = $ttl;
        }

        return $cacheQuery;
    }

    /**
     * Flush model cache
     *
     * @return void
     */
    public static function cacheFlush()
    {
        $self = new static;
        $self->getConnection()->cacheFlush();
    }

    /**
     * Destroy the model with ID
     *
     * @param  string|int  $id
     * @return bool
     */
    public static function destroy( $id )
    {
        $object = self::findOrFail( $id );
        return $object->delete();
    }

    /**
     * Destroy the current model
     *
     * @return bool
     */
    public function delete()
    {
        if( !$this->id )
        {
            return null;
        }

        $endpoint = $this->getEndpoint() . '/' . $this->id;

        $response = $this->getConnection()->sendRequest( $endpoint, 'DELETE' );

        return $response;
    }

    /**
     * Find a model by ID
     *
     * @param  string|int  $id
     * @return object
     */
    public static function find( $id )
    {
        $object = new static;
        $endpoint = $object->getEndpoint() . '/' . $id;

        $response = $object->getConnection()->sendRequest( $endpoint );

        if( $response !== NULL )
        {
            return $object->responseTranslator( $response );
        }

        return $response;
    }

    /**
     * Find a model by ID or throw an exception.
     *
     * @param  string|int  $id
     * @return object
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     */
    public static function findOrFail( $id )
    {
        $object = new static;
        $result = $object->find($id);
        if (is_array($id)) {
            if (count($result) === count(array_unique($id))) {
                return $result;
            }
        } elseif (! is_null($result)) {
            return $result;
        }
        throw (new ModelNotFoundException)->setModel(
            get_class($object->model), $id
        );
    }

    /**
     * Execute the query, send request to API
     *
     * @return mixed|null
     */
    public function get()
    {
        $endpoint = $this->getEndpoint();

        $queryParams = $this->getQuery()->toArray();
        if( $queryParams )
        {
            $this->getConnection()->replaceParameters( $queryParams );
        }

        $response = $this->getConnection()->sendRequest( $endpoint );

        if( $response !== NULL )
        {
            return $this->responseTranslator( $response );
        }
        return NULL;
    }

    public function paginate( $perPage = null, $pageName = 'page', $page = null )
    {
        $page = $page ?: Paginator::resolveCurrentPage($pageName);
        $perPage = $perPage ?: $this->perPage;
        $this->limit( $perPage );
        $this->where( 'page', $page );

        $endpoint = $this->getEndpoint();
        $queryParams = $this->getQuery()->toArray();

        if( $queryParams )
        {
            $this->getConnection()->replaceParameters( $queryParams );
        }
        $response = $this->getConnection()->sendRequest( $endpoint );

        $collectionWrapper = $this->getConfiguration( 'wrappers.collection.root' );
        $responseResults = null;
        if( $collectionWrapper )
        {
            $responseResults = $response;
            foreach( explode( '.', $collectionWrapper ) as $collectionWrapperPart )
            {
                $responseResults = $responseResults->$collectionWrapperPart;
            }
        }
        else
        {
            $responseResults = $response;
        }

        if( $responseResults && is_array($responseResults) )
        {
            $paginationWrappers_total = $this->getConfiguration( 'wrappers.collection.pagination.total' );
            if( $paginationWrappers_total )
            {
                $responseTotal = $response;
                foreach( explode( '.', $paginationWrappers_total ) as $collectionWrapperTotalPart )
                {
                    $responseTotal = $responseTotal->$collectionWrapperTotalPart;
                }
            }
            else
            {
                $responseTotal = count($responseResults);
            }
            $list = [];
            foreach( $responseResults as $responseItem )
            {
                $object = new static;
                $object->forceFill( (array)$responseItem );
                $list[] = $object;
            }
            $itemsList = new LengthAwarePaginator(
                $list,
                $responseTotal,
                $perPage,
                $page,
                [
                    'path'  => request()->url(),
                    'query' => request()->query(),
                ]
            );
            return $itemsList;
        }
        return null;
    }

    /**
     * Send request to API, requesting all model record
     *
     * @return mixed|null
     */
    public static function all()
    {
        $object = new static;
        $endpoint = $object->getEndpoint();

        $object->limit( -1 );

        $queryParams = $object->getQuery()->toArray();
        if( $queryParams )
        {
            $object->getConnection()->replaceParameters( $queryParams );
        }

        $response = $object->getConnection()->sendRequest( $endpoint );
        if( is_object($response) )
        {
            $collectionWrapper = $object->getConfiguration( 'wrappers.collection.root' );
            if( $collectionWrapper )
            {
                if( isset($response->$collectionWrapper) )
                {
                    return $object->responseTranslator( $response->$collectionWrapper );
                }
            }
            return null;
        }
        return $object->responseTranslator( $response );
    }

    /**
     * Send request to API, requesting only first result found
     *
     * @return mixed|null
     */
    public function first()
    {
        $endpoint = $this->getEndpoint();

        $this->limit( 1 );

        $queryParams = $this->getQuery()->toArray();
        if( $queryParams )
        {
            $this->getConnection()->replaceParameters( $queryParams );
        }

        $response = $this->getConnection()->sendRequest( $endpoint );

        $collectionWrapper = $this->getConfiguration( 'wrappers.collection.root' );
        if( $collectionWrapper )
        {
            if( isset($response->$collectionWrapper) && isset($response->$collectionWrapper[0]) )
            {
                return $this->responseTranslator( $response->$collectionWrapper[0] );
            }
        }
        elseif( is_array($response) && isset($response[0]) )
        {
            return $this->responseTranslator( $response[0] );
        }
        return null;
    }

    /**
     * Send request to API
     *
     * @param  array  $endpointExtra
     * @param  string  $method
     * @param  array  $parameters
     * @param  string  $responseProperty
     * @param  array  $cacheConfiguration
     * @return mixed|null
     */
    protected static function call( array $endpointExtra,  string $method = 'GET', array $parameters = [], string $responseProperty = '', array $cacheConfiguration = [] )
    {
        $self = new static;
        $endpoint = $self->getEndpoint();

        if( $endpointExtra )
        {
            if( $endpoint )
            {
                $endpoint .= '/';
            }
            $endpoint .= implode( '/', $endpointExtra );
        }
        if( $parameters )
        {
            $self->getConnection()->replaceParameters( $parameters );
        }
        // overwrite default cache configuration
        if( $cacheConfiguration ) {
            if( array_key_exists( 'cache', $cacheConfiguration ) ) {
                $self->cache = $cacheConfiguration['cache'];
            }
            if( array_key_exists( 'cacheRefresh', $cacheConfiguration ) ) {
                $self->cacheRefresh = $cacheConfiguration['cacheRefresh'];
            }
            if( array_key_exists( 'cacheTtl', $cacheConfiguration ) ) {
                $self->cacheTtl = $cacheConfiguration['cacheTtl'];
            }
        }

        $response = $self->getConnection()->sendRequest( $endpoint, $method );

        if( $responseProperty )
        {
            if( !empty($response->$responseProperty) )
            {
                return $self->responseTranslator( $response->$responseProperty );
            }
        }
        else
        {
            if( $response !== NULL )
            {
                return $self->responseTranslator( $response );
            }
        }
        return null;
    }

}