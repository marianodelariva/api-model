<?php

namespace MdelaRiva\ApiModel\Models\Youtube;

use MdelaRiva\ApiModel\ApiModelAbstract;
use MdelaRiva\ApiModel\Exceptions\MethodNotAllowedException;

class Channel extends ApiModelAbstract
{

    /**
     * {@inheritdoc}
     */
    protected $driver = \MdelaRiva\ApiModel\Drivers\Youtube\Channel::class;

    /**
     * {@inheritdoc}
     */
    protected $endpoint = 'channels';

    /**
     * {@inheritdoc}
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct( $attributes );
        //
        $this->fillable = $this->getQuery()->getWhereParameters();
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        $this->limit( 1 );

        $endpoint = $this->getEndpoint();

        $queryParams = $this->getQuery()->toArray();
        if( $queryParams )
        {
            $this->getConnection()->mergeParameters( $queryParams );
        }

        $response = $this->getConnection()->sendRequest( $endpoint );

        return (int)( $response->pageInfo->totalResults ?: 0 );
    }

    /**
     * {@inheritdoc}
     */
    public static function find( $id )
    {
        $query = self::query();
        $query->where( 'id', $id );
        $query->where( 'part', 'snippet,statistics' );

        return $query->first();
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public static function all()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public static function destroy( $id )
    {
        throw new MethodNotAllowedException();
    }
}