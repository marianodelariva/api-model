<?php

namespace MdelaRiva\ApiModel\Models\Youtube;

use MdelaRiva\ApiModel\ApiModelAbstract;
use MdelaRiva\ApiModel\Exceptions\MethodNotAllowedException;

class Search extends ApiModelAbstract
{

    /**
     * {@inheritdoc}
     */
    protected $driver = \MdelaRiva\ApiModel\Drivers\Youtube\Search::class;

    /**
     * {@inheritdoc}
     */
    protected $endpoint = 'search';

    /**
     * Type of resource: channel
     *
     * @var string
     */
    const TYPE_CHANNEL = 'channel';

    /**
     * Type of resource: playlist
     *
     * @var string
     */
    const TYPE_PLAYLIST = 'playlist';

    /**
     * Type of resource: video
     *
     * @var string
     */
    const TYPE_VIDEO = 'video';

    /**
     * {@inheritdoc}
     */
    public function __construct(array $attributes = [])
    {
        parent::__construct( $attributes );
        //
        $this->fillable = $this->getQuery()->getWhereParameters();
    }

    /**
     * {@inheritdoc}
     */
    public function count()
    {
        $this->limit( 1 );

        $endpoint = $this->getEndpoint();

        $queryParams = $this->getQuery()->toArray();
        if( $queryParams )
        {
            $this->getConnection()->mergeParameters( $queryParams );
        }

        $response = $this->getConnection()->sendRequest( $endpoint );

        return (int)( $response->pageInfo->totalResults ?: 0 );
    }

    /**
     * {@inheritdoc}
     */
    public static function findOrFail( $id )
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public static function find( $id )
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public static function all()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public static function destroy( $id )
    {
        throw new MethodNotAllowedException();
    }
}