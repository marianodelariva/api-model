<?php

namespace MdelaRiva\ApiModel\Models\Twitter;

use MdelaRiva\ApiModel\ApiModelAbstract;
use MdelaRiva\ApiModel\Exceptions\MethodNotAllowedException;

class User extends ApiModelAbstract
{

    /**
     * {@inheritdoc}
     */
    protected $driver = \MdelaRiva\ApiModel\Drivers\Basic::class;

    /**
     * {@inheritdoc}
     */
    protected $endpoint = 'users/show.json';

    /**
     * {@inheritdoc}
     */
    protected $fillable = [
        'user_id',
        'screen_name',
        'include_entities',
    ];

    /**
     * {@inheritdoc}
     */
    public function get()
    {
        $queryParams = $this->getQuery()->toArray();

        if( empty($queryParams['user_id']) && empty($queryParams['screen_name']) )
        {
            throw new MethodNotAllowedException( 'Either an user_id or screen_name is required.' );
        }

        return parent::get();
    }

    /**
     * {@inheritdoc}
     */
    public static function find( $id )
    {
        $query = self::query();
        $query->where( 'user_id', $id );

        return $query->get();
    }

    /**
     * {@inheritdoc}
     */
    public function first()
    {
        return $this->get();
    }


    /**
     * {@inheritdoc}
     */
    public function paginate( $perPage = null, $pageName = 'page', $page = null )
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public static function firstOrNew( array $where )
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public function save()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public static function all()
    {
        throw new MethodNotAllowedException();
    }

    /**
     * {@inheritdoc}
     */
    public static function destroy( $id )
    {
        throw new MethodNotAllowedException();
    }
}