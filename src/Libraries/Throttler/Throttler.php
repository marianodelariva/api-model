<?php

namespace MdelaRiva\ApiModel\Libraries\Throttler;

use Illuminate\Http\Exceptions\ThrottleRequestsException;
use Illuminate\Cache\RateLimiter;
use Illuminate\Support\InteractsWithTime;

class Throttler
{
    use InteractsWithTime;

    /**
     * Request key
     *
     * @var string
     */
    private $key;

    /**
     * Throttler base configuration
     *
     * @var array
     */
    private $configuration = [
        'throw_exception' => FALSE,
        'limits' => [],
    ];

    /**
     * Create a new throttler.
     *
     * @param  string  $baseKey
     * @param  array  $configuration
     * @return void
     */
    public function __construct( string $baseKey, array $configuration = [] )
    {
        // limiter
        $this->limiter = app(RateLimiter::class);
        // main key
        $this->key = $baseKey;
        // configuration
        if( $configuration )
        {
            $this->configuration = array_replace_recursive( $this->configuration, $configuration );
        }
    }

    /**
     * Check request
     *
     * @param  string  $subKey
     * @param  int|null  $maxAttempts
     * @param  int|null  $decaySeconds
     * @return bool
     *
     * @throws \Illuminate\Http\Exceptions\ThrottleRequestsException
     */
    public function check( string $subKey = '', int $maxAttempts = null, int $decaySeconds = null )
    {
        $limits = [];
        if( $maxAttempts && $decaySeconds )
        {
            $limits[] = [
                'max_attempts' => $maxAttempts,
                'decay_seconds' => $decaySeconds,
            ];
        }
        else
        {
            $limits = $this->getConfiguration( 'limits' );
        }
        if( !$limits )
        {
            return TRUE;
        }
        if( !is_array($limits) )
        {
            throw new ThrottleRequestsException( 'invalid limit' );
        }
        $limitFirstError = [];
        foreach( $limits as $limit )
        {
            if( empty($limit['max_attempts']) || empty($limit['decay_seconds']) )
            {
                throw new ThrottleRequestsException( 'invalid limit: ' . json_encode( $limit ) );
            }
            $key = $this->resolveSignature( $limit['max_attempts'], $limit['decay_seconds'], $subKey );
            if ($this->limiter->tooManyAttempts( $key, $limit['max_attempts'] ) )
            {
                if( !$limitFirstError )
                {
                    $limitFirstError = $limit;
                }
            }
            else
            {
                $this->limiter->hit( $key, $limit['decay_seconds'] );
            }
        }

        if( $limitFirstError )
        {
            if( $this->getConfiguration( 'throw_exception' ) === TRUE )
            {
                $keyFirstError = $this->resolveSignature( $limitFirstError['max_attempts'], $limitFirstError['decay_seconds'], $subKey );
                throw $this->buildException( $keyFirstError, $limitFirstError['max_attempts'], $limitFirstError['decay_seconds'] );
            }
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Returns specific key or entire cache configuration
     *
     * @param  string|null  $key
     * @return mixed
     */
    private function getConfiguration( string $key = null )
    {
        $configuration = $this->configuration;
        if( $key )
        {
            foreach( explode( '.', $key ) as $keyPart )
            {
                $configuration = ( $configuration[$keyPart] ?? NULL );
                if( $configuration === NULL )
                {
                    return NULL;
                }
            }
        }
        return $configuration;
    }

    /**
     * Resolve request signature.
     *
     * @param  int  $maxAttempts
     * @param  int  $decaySeconds
     * @param  string  $subKey
     * @return string
     *
     * @throws \RuntimeException
     */
    protected function resolveSignature( int $maxAttempts, int $decaySeconds, string $subKey = '' )
    {
        $key = $this->key . '_' . $maxAttempts . '_' . $decaySeconds;
        if( $subKey )
        {
            $key .= '__' . $subKey;
        }
        $key = sha1( 'throttler__' . $key );
        return $key;
    }

    /**
     * Create a 'too many attempts' exception.
     *
     * @param  string  $key
     * @param  int  $maxAttempts
     * @param  int  $decaySeconds
     * @return \Illuminate\Http\Exceptions\ThrottleRequestsException
     */
    protected function buildException( string $key, int $maxAttempts, int $decaySeconds )
    {
        $retryAfter = $this->getTimeUntilNextRetry( $key );

        $headers = $this->getHeaders(
            $maxAttempts,
            $this->calculateRemainingAttempts( $key, $maxAttempts, $retryAfter ),
            $retryAfter
        );

        return new ThrottleRequestsException(
            'Too Many Attempts (Limit: ' . $maxAttempts . ' every ' . $decaySeconds . 'sec).',
            null,
            $headers
        );
    }

    /**
     * Get the limit headers information.
     *
     * @param  int  $maxAttempts
     * @param  int  $remainingAttempts
     * @param  int|null  $retryAfter
     * @return array
     */
    protected function getHeaders( int $maxAttempts, int $remainingAttempts, int $retryAfter = null)
    {
        $headers = [
            'X-RateLimit-Limit' => $maxAttempts,
            'X-RateLimit-Remaining' => $remainingAttempts,
        ];

        if( !is_null($retryAfter) )
        {
            $headers['Retry-After'] = $retryAfter;
            $headers['X-RateLimit-Reset'] = $this->availableAt( $retryAfter );
        }

        return $headers;
    }

    /**
     * Get the number of seconds until the next retry.
     *
     * @param  string  $key
     * @return int
     */
    protected function getTimeUntilNextRetry( string $key )
    {
        return $this->limiter->availableIn( $key );
    }

    /**
     * Calculate the number of remaining attempts.
     *
     * @param  string  $key
     * @param  int  $maxAttempts
     * @param  int|null  $retryAfter
     * @return int
     */
    protected function calculateRemainingAttempts( string $key, int $maxAttempts, int $retryAfter = null )
    {
        if( is_null($retryAfter) )
        {
            return $this->limiter->retriesLeft( $key, $maxAttempts );
        }

        return 0;
    }

}