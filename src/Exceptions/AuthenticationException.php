<?php

namespace MdelaRiva\ApiModel\Exceptions;

use Exception;

class AuthenticationException extends Exception
{
    protected $message = 'Unauthenticated.';
}