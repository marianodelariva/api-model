<?php

namespace MdelaRiva\ApiModel\Exceptions;

use Exception;

class MissingConfigurationException extends Exception
{
    protected $message = 'Missing configuration parameter.';
}